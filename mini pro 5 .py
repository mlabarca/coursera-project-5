# implementation of card game - Memory

import simplegui
import random

deck = range(8)+range(8)
cards = simplegui.load_image("http://dl.dropbox.com/s/lt3b4yfe329wkv9/cards%20v2.jpg")
#Storing image coords in a dict. This is not necessary but i wanted to practice dicts
coords = {0:[75,50],1:[125,50],2:[175,50],3:[225,50],4:[275,50],5:[325,50],6:[
          375,50],7:[425,50]}
#Sorry for the low quality images, sizing the cards to 50x100 limited options...
#Should have searched for images first before doing the layout :/ 
# helper function to initialize globals
def init():
    global state,exposed,match,moves,mode
    state = 0
    moves = 0
    random.shuffle(deck)
    exposed = [False,False,False,False,False,False,False,False,
           False,False,False,False,False,False,False,False]
    match = False
    mode = False   
def mode():
    """Changes mode from graphic to numeric. """
    global mode
    if mode:
        mode = False
    else:
        mode = True
# define event handlers
def mouseclick(pos):
    global state,card_1,card_2,match,moves
    index = pos[0]//50 
    if exposed[index] == False:
        exposed[index] = True
        if state == 0:
            state = 1
            card_1 = index
        elif state == 1:
            state = 2
            card_2 = index
            moves +=1
        else:
            state = 1
            if match == False:
                exposed[card_1] = False
                exposed[card_2] = False
            card_1 = index           
def draw(canvas):
    # cards are 50x100 pixels in size
    canvas.draw_text("Try the image mode!",[20,117],13,"White","sans-serif")
    canvas.draw_text("(This space used to give more space to control area)",[495,117],13,"White","sans-serif")
    global match,mode
    #Matching logic 
    if state == 2:
        if deck[card_1] == deck[card_2]:
            match = True
        else:
            match = False
    #Moves label        
    label.set_text("Moves = "+str(moves))      
    # Main drawing loop
    positioner = 0
    for card in deck:
        card_loc = [25+50*positioner,55]
        poly_loc = [[card_loc[0]-25,0],[card_loc[0]+25,0],[
                    card_loc[0]+25,99],[card_loc[0]-25,99]]
        if exposed[positioner]:
            if mode:
                canvas.draw_image(cards,coords[card], [50,100], card_loc, [50,100])
            else:
                canvas.draw_text(str(card),card_loc,17,"White","sans-serif")
        else:
            if mode:
                canvas.draw_image(cards,[25,50], [50,100], card_loc, [50,100])
            else:
                canvas.draw_polygon(poly_loc, 1, "White", "#6495ED")
          
        positioner += 1
    #These two lines enable automatic restart at the end
    #if all(exposed):
        #init()
# create frame and add a button and labels
frame = simplegui.create_frame("Memory", 800, 120,300)
frame.add_button("Restart", init)
label = frame.add_label("Moves = 0")
frame.add_button("Image mode?", mode)
# initialize global variables
init()
# register event handlers
frame.set_mouseclick_handler(mouseclick)
frame.set_draw_handler(draw)
# get things rolling
frame.start()


